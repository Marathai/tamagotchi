from status_potrzeb import aktuale_potrzeby
from jedzenie import lista_jedzenia
from zabawa import lista_zabaw
from pielegnacja import lista_pielegnacyjna
from sprawdzanie_inputu import sprawdz_input


def opcje_opieki(wybrana_opcja, aktualny_zwierzak):
    if wybrana_opcja == 1:
        aktualizacja_potrzeb(aktualny_zwierzak)
        aktuale_potrzeby(aktualny_zwierzak)

    elif wybrana_opcja == 2:
        aktualizacja_potrzeb(aktualny_zwierzak)
        print("Czym chcesz nakarmic zwierzaka? ")
        licznik = 0
        for jedzenie in lista_jedzenia:
            licznik += 1
            print(licznik, ":", jedzenie.nazwa_jezenia)
        wybrany_pokarm = sprawdz_input("Wybierz numerek z jedzeniem: ", lista_jedzenia.__len__())
        aktualny_zwierzak.glod = karmienie(aktualny_zwierzak, lista_jedzenia[wybrany_pokarm - 1])

    elif wybrana_opcja == 3:
        aktualizacja_potrzeb(aktualny_zwierzak)
        print("Jak chcesz bawic sie ze zwierzakiem?")
        licznik = 0
        for zabawy in lista_zabaw:
            licznik += 1
            print(licznik, ":", zabawy.nazwa_zabawy)
        wybrana_zabawa = sprawdz_input("Wybierz numerek z zabawa: ", lista_zabaw.__len__())
        aktualny_zwierzak.zabawa = zabawa(aktualny_zwierzak, lista_zabaw[wybrana_zabawa - 1])

    elif wybrana_opcja == 4:
        aktualizacja_potrzeb(aktualny_zwierzak)
        print("Jaki zabieg pielegnacyjny wybierzesz dla zawierzaka?")
        licznik = 0
        for zabieg in lista_pielegnacyjna:
            licznik += 1
            print(licznik, ":", zabieg.nazwa_zabiegu)
        wybrany_zabieg = sprawdz_input("Wybierz numerek z zabiegiem pielegnacyjnym: ", lista_pielegnacyjna.__len__())
        aktualny_zwierzak.higiena = pielegnowanie(aktualny_zwierzak, lista_pielegnacyjna[wybrany_zabieg - 1])

    if aktualny_zwierzak.glod > 7 or aktualny_zwierzak.zabawa > 8:
        aktualny_zwierzak.daj_glos()


def aktualizacja_potrzeb(zwierze):
    uplynelo = zwierze.aktualizuj_czas_dla_potrzeb()
    zwierze.wzrost_glodu(uplynelo)
    zwierze.potrzeba_zabawy(uplynelo)
    zwierze.potrzeba_higieny(uplynelo)

    if zwierze.glod > 7 or zwierze.zabawa > 8:
        zwierze.daj_glos()


def karmienie(zwierze, jedzenie):
    zwierze.glod = zwierze.glod - jedzenie.nasycanie
    if zwierze.glod < 0:
        zwierze.glod = 0
    return zwierze.glod


def zabawa(zwierze, zajecie):
    zwierze.zabawa = zwierze.zabawa - zajecie.punkty_zabawy
    if zwierze.zabawa < 0:
        zwierze.zabawa = 0
    return zwierze.zabawa


def pielegnowanie(zwierze, pielegnacja):
    zwierze.higiena = zwierze.higiena - pielegnacja.punkty_higieny
    if zwierze.higiena < 0:
        zwierze.higiena = 0
    return zwierze.higiena
