import time
from zwierze import Zwierze, Kot, Pies
from sprawdzanie_inputu import sprawdz_input
from akcje_uzytkownika import opcje_opieki

kitku = Kot(0, 0, 0)
piesel = Pies(0, 0, 0)
dostepne_zwierzeta = [kitku, piesel]


def Menu():
    print("Witaj w grze Tamagotchi")
    print ("Wybierz cyferke zwierzatka ktorym sie dzis zaopiekujesz:")

    licznik = 0
    for zwierze in dostepne_zwierzeta:
        licznik += 1
        print(licznik,":",zwierze.nazwa)
    wybrany_numer = sprawdz_input("", dostepne_zwierzeta.__len__())

    return wybrany_numer

def Menu_zwierzakow(wybrano):

    aktualny_zwierzak = dostepne_zwierzeta[wybrano - 1]
    aktualny_zwierzak.ostatnia_aktualizacja_potrzeb = time.time()
    print("\nWybrales", aktualny_zwierzak.nazwa)

    while True:
        print("Co chcesz zrobic? ")
        wybrana_opcja = sprawdz_input("\n1: Wyswietl status zwierzecia\n2: Nakarm\n3: Pobaw sie\n4: Pielegnacja: \n5: Wyjdz", 5)
        opcje_opieki(wybrana_opcja, aktualny_zwierzak)

        if wybrana_opcja == 5:
            break



