def sprawdz_input(wiadomosc, zakres):

    while True:
        try:
            wejscie = int(input(wiadomosc))
        except:
            print("wybrany element musi byc liczba")
            continue
        if wejscie <= 0 or wejscie > zakres:
            print("przekoroczyles dostepny zakres liczobwy")
            continue
        break

    return wejscie