import time, json
doba = 24

class Zwierze:
    def __init__(self, glod, higiena, zabawa, nazwa):
        self.glod = glod
        self.zabawa = zabawa
        self.higiena = higiena
        self.nazwa = nazwa
        self.ostatnia_aktualizacja_potrzeb = time.time()

    def aktualizuj_czas_dla_potrzeb(self):
        czas_teraz = time.time()
        minelo = self.ostatnia_aktualizacja_potrzeb - czas_teraz
        self.ostatnia_aktualizacja_potrzeb = czas_teraz
        return int(minelo)


class Kot(Zwierze):
    def __init__(self, glod, higiena, zabawa):
        Zwierze.__init__(self, glod, higiena, zabawa, "Kot")

    def wzrost_glodu(self, odstep_czasowy):
        self.glod = self.glod + int((odstep_czasowy * (1/2))/doba)

    def potrzeba_zabawy(self, odstep_czasowy):
        self.zabawa = self.zabawa + int((odstep_czasowy * (1 / 4))/doba)

    def potrzeba_higieny(self, odstep_czasowy):
        self.higiena = self.higiena + int(odstep_czasowy * (1/10)/doba)

    def daj_glos(self):
        print("Miaau!")


class Pies(Zwierze):
    def __init__(self, glod, higiena, zabawa):
        Zwierze.__init__(self, glod, higiena, zabawa, "Pies")

    def wzrost_glodu(self, odstep_czasowy):
        self.glod = self.glod + int((odstep_czasowy * (1/4))/doba)

    def potrzeba_zabawy(self, odstep_czasowy):
        self.zabawa = self.zabawa + int((odstep_czasowy * (1 / 3))/doba)

    def potrzeba_higieny(self, odstep_czasowy):
        self.higiena = self.higiena + int((odstep_czasowy * (1/8)/doba))

    def daj_glos(self):
        print("Hau!")
